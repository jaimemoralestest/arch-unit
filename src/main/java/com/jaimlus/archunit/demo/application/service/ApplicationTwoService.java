package com.jaimlus.archunit.demo.application.service;

import com.jaimlus.archunit.demo.domain.port.DomainTwoPort;
import org.springframework.stereotype.Service;

@Service
public class ApplicationTwoService {

	private DomainTwoPort domainTwoPort;

	public ApplicationTwoService(DomainTwoPort domainTwoPort) {
		this.domainTwoPort = domainTwoPort;
	}

	public void methodTwo() {
		this.domainTwoPort.methodTwo();
		System.out.println("Method two");
	}
}
