package com.jaimlus.archunit.demo.application.dto.response;

public class BaseResponse {

	private Object data;
	private Object notification;

	public BaseResponse(Object data, Object notification) {
		this.data = data;
		this.notification = notification;
	}

}
