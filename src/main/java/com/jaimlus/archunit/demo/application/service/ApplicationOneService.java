package com.jaimlus.archunit.demo.application.service;

import com.jaimlus.archunit.demo.domain.port.DomainOnePort;
import org.springframework.stereotype.Service;

@Service
public class ApplicationOneService {

	private DomainOnePort domainOnePort;

	public ApplicationOneService(DomainOnePort domainOnePort) {
		this.domainOnePort = domainOnePort;
	}

	public void methodOne() {
		this.domainOnePort.methodOne();
		System.out.println("Method one");
	}
}
