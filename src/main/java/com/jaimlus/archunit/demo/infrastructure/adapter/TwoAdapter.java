package com.jaimlus.archunit.demo.infrastructure.adapter;

import com.jaimlus.archunit.demo.domain.port.DomainTwoPort;
import org.springframework.stereotype.Component;

@Component
public class TwoAdapter implements DomainTwoPort {

	@Override
	public void methodTwo() {
		System.out.println("Method one");
	}
}
