package com.jaimlus.archunit.demo.infrastructure.adapter;

import com.jaimlus.archunit.demo.domain.port.DomainOnePort;
import org.springframework.stereotype.Component;

@Component
public class OneAdapter implements DomainOnePort {

	@Override
	public void methodOne() {
		System.out.println("Method one");
	}
}
