package com.jaimlus.archunit.demo.infrastructure.controller;

import com.jaimlus.archunit.demo.application.dto.response.BaseResponse;
import com.jaimlus.archunit.demo.application.service.ApplicationOneService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/demo/archunit")
public class OneController {

	private ApplicationOneService applicationOneService;

	public OneController(
		ApplicationOneService applicationOneService) {
		this.applicationOneService = applicationOneService;
	}

	@GetMapping(path = "/method-one")
	public BaseResponse methodOne() {
		this.applicationOneService.methodOne();

		return new BaseResponse(null, null);
	}

}
