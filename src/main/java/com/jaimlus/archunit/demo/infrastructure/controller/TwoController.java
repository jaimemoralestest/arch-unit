package com.jaimlus.archunit.demo.infrastructure.controller;

import com.jaimlus.archunit.demo.application.dto.response.BaseResponse;
import com.jaimlus.archunit.demo.application.service.ApplicationTwoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/demo/archunit")
public class TwoController {

	private ApplicationTwoService applicationTwoService;

	public TwoController(
		ApplicationTwoService applicationTwoService) {
		this.applicationTwoService = applicationTwoService;
	}

	@GetMapping(path = "/method-two")
	public BaseResponse methodTwo() {
		this.applicationTwoService.methodTwo();

		return new BaseResponse(null, null);
	}

}
