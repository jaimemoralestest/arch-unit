package com.jaimlus.archunit.demo.domain.service;

import org.springframework.stereotype.Service;

@Service
public class DomainOneService {

	public DomainOneService() {
	}

	public void methodOne() {
		System.out.println("Method one");
	}

}
