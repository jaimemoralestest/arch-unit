package com.jaimlus.archunit.demo.domain.port;

public interface DomainOnePort {

	void methodOne();

}
