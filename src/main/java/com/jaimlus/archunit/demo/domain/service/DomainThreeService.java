package com.jaimlus.archunit.demo.domain.service;

import com.jaimlus.archunit.demo.domain.port.DomainThreePort;
import org.springframework.stereotype.Service;

@Service
public class DomainThreeService {

	private DomainThreePort domainThreePort;

	public DomainThreeService(DomainThreePort domainThreePort) {
		this.domainThreePort = domainThreePort;
	}

	public void methodThree(){
		this.domainThreePort.methodThree();
	}
}
