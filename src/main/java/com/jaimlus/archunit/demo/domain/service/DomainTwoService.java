package com.jaimlus.archunit.demo.domain.service;

import org.springframework.stereotype.Service;

@Service
public class DomainTwoService {

	private DomainOneService domainOneService;

	public DomainTwoService(DomainOneService domainOneService) {
		this.domainOneService = domainOneService;
	}

	public void methodOne() {
		this.domainOneService.methodOne();
		System.out.println("Method one");
	}
}
