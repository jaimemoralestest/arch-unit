package architecture.layer;

import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo",
	importOptions = ImportOption.DoNotIncludeTests.class
)
public class LayerChecksTest {

	@ArchTest
	ArchRule access_layers_control = layeredArchitecture()
		.layer("infrastructure").definedBy("..infrastructure..")
		.layer("domain").definedBy("..domain..")
		.layer("application").definedBy("..application..")

		.whereLayer("infrastructure").mayNotBeAccessedByAnyLayer()
		.whereLayer("application").mayOnlyBeAccessedByLayers("infrastructure")
		.whereLayer("domain").mayOnlyBeAccessedByLayers("infrastructure", "application");

}
