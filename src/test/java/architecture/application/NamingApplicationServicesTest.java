package architecture.application;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo.application",
	importOptions = ImportOption.DoNotIncludeTests.class
)
public class NamingApplicationServicesTest {

	@ArchTest
	ArchRule application_services_should_be_suffixed = classes()
		.that().resideInAPackage("..service..")
		.and().areNotAnonymousClasses()
		.should().haveSimpleNameEndingWith("Service")
		.because("The application services should end with Service");

}
