package architecture.domain;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo",
	importOptions = ImportOption.DoNotIncludeTests.class
)
public class ClassAccessDomainTest {

	@ArchTest
	public ArchRule class_acces_domain_port = classes()
		.that().resideInAPackage("..domain.port..")
		.and().areInterfaces()
		.should().onlyBeAccessed().byClassesThat().resideInAnyPackage(
			"..application.service..", "..domain.service..")
		.because("The domain ports can be access to application services or domain services");

}
