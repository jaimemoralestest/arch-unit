package architecture.domain;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo.domain",
	importOptions = ImportOption.DoNotIncludeTests.class
)
public class NamingDomainPortTest {

	@ArchTest
	ArchRule domain_port_should_be_suffixed = classes()
		.that().resideInAPackage("..port..")
		.should().haveSimpleNameEndingWith("Port")
		.because("The domain ports should end with Port");

}
