package architecture.domain;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;
import org.springframework.stereotype.Service;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo.domain"
)
public class AnnotatedDomainServicesTest {

	@ArchTest
	public ArchRule domain_services_should_annoted_with_service = classes()
		.that().resideInAPackage("..service..")
		.and().areNotAnonymousClasses()
		.should().beAnnotatedWith(Service.class)
		.because("Domain services should annoted with @Service");
}
