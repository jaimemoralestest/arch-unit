package architecture.infrastructure;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo",
	importOptions = ImportOption.DoNotIncludeTests.class
)
public class ClassImplementAdapterTest {

	@ArchTest
	public ArchRule class_implement_adapter = classes()
		.that().resideInAPackage("..infrastructure.adapter..")
		.should().implement(JavaClass.Predicates.resideInAPackage("..domain.port.."))
		.because("The adapters can only be accessed with domain ports");

}
