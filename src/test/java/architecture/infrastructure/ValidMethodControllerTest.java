package architecture.infrastructure;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;

import com.jaimlus.archunit.demo.application.dto.response.BaseResponse;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo.infrastructure"
)
public class ValidMethodControllerTest {

	@ArchTest
	ArchRule annotated_methods_controller = methods()
		.that().arePublic()
		.and().areDeclaredInClassesThat()
			.resideInAPackage("..controller")
		.and().areDeclaredInClassesThat()
			.haveSimpleNameEndingWith("Controller")
		.and().areDeclaredInClassesThat()
			.areAnnotatedWith(RestController.class)
		.or().areDeclaredInClassesThat()
			.areAnnotatedWith(RequestMapping.class)
		.should().beAnnotatedWith(GetMapping.class)
		.orShould().beAnnotatedWith(PostMapping.class)
		.orShould().beAnnotatedWith(PatchMapping.class)
		.orShould().beAnnotatedWith(PutMapping.class)
		.orShould().beAnnotatedWith(DeleteMapping.class)
		.because("Controller methods should be annotated with http methods");


	@ArchTest
	ArchRule return_baseResponse_methods_controller = methods()
		.that().arePublic()
		.and().areDeclaredInClassesThat()
			.resideInAPackage("..controller")
		.and().areDeclaredInClassesThat()
			.haveSimpleNameEndingWith("Controller")
		.and().areDeclaredInClassesThat()
			.areAnnotatedWith(RestController.class)
		.or().areDeclaredInClassesThat()
			.areAnnotatedWith(RequestMapping.class)
		.should().haveRawReturnType(BaseResponse.class)
		.because("Controller methods should return BaseResponse object");

}
