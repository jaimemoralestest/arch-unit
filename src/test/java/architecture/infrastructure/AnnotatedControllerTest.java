package architecture.infrastructure;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;
import org.springframework.web.bind.annotation.RestController;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo.infrastructure",
	importOptions = ImportOption.DoNotIncludeTests.class
)
public class AnnotatedControllerTest {

	@ArchTest
	ArchRule controllers_should_annoted_with_rest_controller = classes()
		.that().resideInAPackage("..controller..")
		.and().areNotAnonymousClasses()
		.should().beAnnotatedWith(RestController.class)
		.because("Controllers should annoted with @RestController");
}
