package architecture.infrastructure;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo.infrastructure",
	importOptions = ImportOption.DoNotIncludeTests.class
)
public class NamingControllerTest {

	@ArchTest
	ArchRule controllers_should_be_suffixed = classes()
		.that().resideInAPackage("..controller..")
		.and().areNotAnonymousClasses()
		.should().haveSimpleNameEndingWith("Controller")
		.because("Controllers should end with Controller");
}