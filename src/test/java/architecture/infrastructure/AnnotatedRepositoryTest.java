package architecture.infrastructure;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.runner.RunWith;
import org.springframework.stereotype.Repository;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
	packages = "com.jaimlus.archunit.demo.infrastructure",
	importOptions = ImportOption.DoNotIncludeTests.class
)
public class AnnotatedRepositoryTest {

	@ArchTest
	ArchRule repository_jpa_should_annoted = classes()
		.that().resideInAPackage("..adapter.repository.jpa..")
		.should().beAnnotatedWith(Repository.class)
		.because("Repositories jpa should annotated with @Repository");

}
